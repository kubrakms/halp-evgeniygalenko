﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using SoftServe.Halp.CalculatorModule.Controllers;

namespace CalculatorModuleTest
{
    [TestFixture]
    public class UtilControllerTest
    {
        [Test(Description="Test for fpTransform invoking by UtilController")]
        [TestCaseSource("TransformParameters")]
        public void UtilControllerTransformTest(int expected, string[] args)
        {
            int actual = 5;
            using (var controller = new UtilController())
            {
                actual = controller.ExecuteUtil(eUtil.fpTransform, args);
                Assert.AreEqual(expected, actual);
            }
        }

        [Test(Description = "Test for fpFormat invoking by UtilController")]
        [TestCaseSource("FormatParameters")]
        public void UtilControllerFormatTest(int expected, string[] args)
        {
            int actual = 5;
            using (var controller = new UtilController())
            {
                actual = controller.ExecuteUtil(eUtil.fpFormat, args);
                Assert.AreEqual(expected, actual);
            }
        }

        #region TestCaseData

        /// <summary>
        /// Represents set of parameters for fpFormat execution
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<TestCaseData> FormatParameters()
        {
            int expected = 0;
            List<string> parameters = new List<string>();
            parameters.Add(@"in:D:\InputTest.csv");
            parameters.Add("from:csv");
            parameters.Add(@"out:D:\OutputFormatTest.json");
            parameters.Add("to:json");

            yield return new TestCaseData(expected, parameters.ToArray());

            expected = 21312;
            parameters = new List<string>();
            parameters.Add(@"in:D:\InputTestInvalid.json");
            parameters.Add("from:json");
            parameters.Add(@"out:D:\OutputFormatTest.bin");
            parameters.Add("to:binary");

            yield return new TestCaseData(expected, parameters.ToArray());
        }

        /// <summary>
        /// Represents set of parameters for fpTransform execution
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<TestCaseData> TransformParameters()
        {
            int expected = 0;
            List<string> parameters = new List<string>();
            parameters.Add(@"in:D:\InputTest.csv");
            parameters.Add(@"out:D:\OutputTransformTest.csv");

            yield return new TestCaseData(expected, parameters.ToArray());

            expected = 21312;
            parameters = new List<string>();
            parameters.Add(@"in:D:\Inpudd43fqIH.csv");
            parameters.Add(@"out:D:\fuck.txt");

            yield return new TestCaseData(expected, parameters.ToArray());
        }

        #endregion
    }
}
