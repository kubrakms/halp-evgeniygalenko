﻿using System;
using NUnit.Framework;
using SoftServe.Halp.EngineerHalp;
using SoftServe.Halp.EngineerHalp.Abstractions;
using System.Globalization;
using System.IO;

namespace EngineerHalpTest
{
    [TestFixture]
    public class IOControllerTest
    {
        [Test]
        public void LocalizationTest()
        {
            using (var io_controller = LogicContainer.Instance.GetService<IIOContract>(new object[]{new CultureInfo("ru")}))
            {
                Assert.IsTrue(io_controller.GetProgramHelp().Contains("Помощь"));
            }
        }

        [Test]
        public void FileCreationTest()
        {
            string path = @"D:\CreatingFileTest.shit";
            using (var io_controller = LogicContainer.Instance.GetService<IIOContract>(new object[] { new CultureInfo("ru") }))
            {
                io_controller.CreateFile(path);
                Assert.IsTrue(File.Exists(path));
            }
            File.Delete(path);
        }
    }
}
