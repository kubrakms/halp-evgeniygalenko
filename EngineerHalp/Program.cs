﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------

namespace EngineerHalp
{
    using SoftServe.Halp.CalculatorModule.Abstractions;
    using SoftServe.Halp.CalculatorModule.Controllers;
    using SoftServe.Halp.EngineerHalp;
    using SoftServe.Halp.EngineerHalp.Abstractions;
    using SoftServe.Halp.EngineerHalp.Resources;
    using System;
    using System.Threading;

    class Program
    {
        static void Main(string[] args)
        {
            const string VER = "ver";
            const string HELP = "help";
            const string COMPUTE = "compute";
            const string TRANSFORM = "transform";
            const string CREATE = "create";
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ru");
            using (var inputController = LogicContainer.Instance.GetService<IIOContract>())
            {
                while (!inputController.IsExit)
                {
                    Console.WriteLine(inputController.GetProgramHelp());

                    var command = inputController.GetCommand();

                    if (string.Compare(command.Name, VER, true) == 0)
                    {
                        Console.WriteLine(inputController.GetVersion());
                    }
                    if (string.Compare(command.Name, HELP, true) == 0)
                    {
                        continue;
                    }

                    if (string.Compare(command.Name, COMPUTE, true) == 0)
                    {
                        int result;
                        using (var utilController = LogicContainer.Instance.GetService<IUtilContract>())
                        {
                            result = utilController.ExecuteUtil(eUtil.fpTransform, command.Arguments);
                        }
                        if (result.Equals(0))
                        {
                            Console.WriteLine(HalpResource.Success);
                        }
                        else
                        {
                            Console.WriteLine(HalpResource.Error);
                        }
                    }

                    if (string.Compare(command.Name, TRANSFORM, true) == 0)
                    {
                        int result;
                        using (var utilController = LogicContainer.Instance.GetService<IUtilContract>())
                        {

                            result = utilController.ExecuteUtil(eUtil.fpFormat, command.Arguments);
                        }
                        if (result.Equals(0))
                        {
                            Console.WriteLine(HalpResource.Success);
                        }
                        else
                        {
                            Console.WriteLine(HalpResource.Error);
                        }
                    }

                    if (string.Compare(command.Name, CREATE, true) == 0)
                    {
                        string fileName = "untitled.csv";

                        if (command.Arguments[0] != null)
                        {
                            fileName = command.Arguments[0];
                        }

                        inputController.CreateFile(fileName);
                        Console.WriteLine(HalpResource.FileCreated);
                    }
                }                                
            }
        }
    }
}
