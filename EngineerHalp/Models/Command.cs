﻿// -----------------------------------------------------------------------
// <copyright file="Command.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Halp.EngineerHalp.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Represents user command with arguments
    /// </summary>
    public class Command
    {
        public string Name { get; set; }
        public string[] Arguments { get; set; }
    }
}