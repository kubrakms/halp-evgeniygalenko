﻿// -----------------------------------------------------------------------
// <copyright file="IOController.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Halp.EngineerHalp.Controllers
{
    using SoftServe.Halp.EngineerHalp.Abstractions;
    using SoftServe.Halp.EngineerHalp.Models;
    using SoftServe.Halp.EngineerHalp.Resources;
    using System;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Threading;

    /// <summary>
    /// 
    /// </summary>
    public class IOController : IIOContract
    {
        public IOController()
        {
            HalpResource.Culture = Thread.CurrentThread.CurrentUICulture;
        }

        public IOController(CultureInfo culture)
        {
            if (culture != null)
                HalpResource.Culture = culture;
            else
                throw new ArgumentNullException("culture");
        }

        /// <summary>
        /// Asks for command input from cmd
        /// </summary>
        /// <returns></returns>
        public Command GetCommand()
        {
            Console.Write(HalpResource.Prompt);
            var inputArray = Console.ReadLine().Split(' ');
            string[] tempArray = new string[5];
            if (inputArray.Length > 1)
            {
                Array.Copy(inputArray, 1, tempArray, 0, inputArray.Length - 1);
            }

            var command = new Command();
            command.Name = inputArray[0];

            if (tempArray.Length != 0)
            {
                command.Arguments = tempArray;
            }

            m_exit = (string.Compare(command.Name, HalpResource.Exit, true) == 0);

            return command;
        }

        /// <summary>
        /// Returns program help
        /// </summary>
        /// <returns></returns>
        public string GetProgramHelp()
        {
            return HalpResource.Help;
        }

        /// <summary>
        /// Returns program version
        /// </summary>
        /// <returns></returns>
        public string GetVersion()
        {
            return Assembly.GetEntryAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Creates file with appropriate filename
        /// </summary>
        /// <param name="fileName"></param>
        public void CreateFile(string fileName)
        {
            File.Create(fileName).Close();
        }

        /// <summary>
        /// Returns true, if exit was entered by user
        /// </summary>
        public bool IsExit
        {
            get { return m_exit; }
        }

        #region IDisposable Implementation

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposeManaged)
        {
            if (m_disposed)
                return;

            if (disposeManaged)
            {

            }

            m_disposed = true;
        }

        #endregion

        private bool m_disposed;
        private bool m_exit;
    }
}