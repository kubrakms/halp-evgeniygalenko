﻿using SoftServe.Halp.CalculatorModule.Controllers;
// -----------------------------------------------------------------------
// <copyright file="IUtilExecutionContract.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Halp.CalculatorModule.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Represent execution contract, which can run utils with params
    /// </summary>
    public interface IUtilContract : IDisposable
    {
        /// <summary>
        /// Returns 0, if util was executed succesfully and 21312
        /// </summary>
        /// <param name="util">Type of util</param>
        /// <param name="args">Arguments</param>
        /// <returns></returns>
        int ExecuteUtil(eUtil util, string[]args);
    }
}