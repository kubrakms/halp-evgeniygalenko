﻿// -----------------------------------------------------------------------
// <copyright file="UtilController.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Halp.CalculatorModule.Controllers
{
    using SoftServe.Halp.CalculatorModule.Abstractions;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// 
    /// </summary>
    public class UtilController : IUtilContract
    {
        const string FP_FORMAT =  "Task5.fpFormat.exe";
        const string FP_TRANSFORM = "fpTransform.exe";

        /// <summary>
        /// Returns 0, if util was executed succesfully, 21312 if error in utils uccurs
        /// </summary>
        /// <param name="util">Type of util</param>
        /// <param name="args">Arguments</param>
        /// <returns></returns>
        public int ExecuteUtil(eUtil util, string[] args)
        {
            if (args != null | File.Exists(FP_FORMAT) | !File.Exists(FP_TRANSFORM))
            {
                StringBuilder tmp = new StringBuilder();
                foreach (var item in args)
                    tmp.Append(string.Format("{0} ", item));

                switch (util)
                {
                    case eUtil.fpTransform:
                        {
                            Process proc = Process.Start(FP_TRANSFORM, tmp.ToString());
                            proc.WaitForExit();
                            int result = proc.ExitCode;
                            return result;
                        }
                    case eUtil.fpFormat:
                        {
                            Process proc = Process.Start(FP_FORMAT, tmp.ToString());
                            proc.WaitForExit();
                            int result = proc.ExitCode;
                            return result;
                        }
                    default: return -1;
                }
            }
            else
                return -1;
        }

        #region IDisposable Implementation

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposeManaged)
        {
            if (m_disposed)
                return;

            if (disposeManaged)
            {

            }

            m_disposed = true;
        }

        #endregion

        private bool m_disposed;
    }
}