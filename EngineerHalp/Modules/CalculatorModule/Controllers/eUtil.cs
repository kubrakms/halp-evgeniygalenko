﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.Halp.CalculatorModule.Controllers
{
    /// <summary>
    /// Represents utils enumeration
    /// </summary>
    public enum eUtil
    {
        fpTransform,
        fpFormat
    }
}
