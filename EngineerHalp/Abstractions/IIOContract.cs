﻿// -----------------------------------------------------------------------
// <copyright file="IInputContract.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Halp.EngineerHalp.Abstractions
{
    using SoftServe.Halp.EngineerHalp.Models;
    using System;

    /// <summary>
    /// Contract for input/output interaction with filesystem and user
    /// </summary>
    public interface IIOContract : IDisposable
    {
        /// <summary>
        /// Asks for command input from cmd
        /// </summary>        
        /// <returns></returns>
        Command GetCommand();        

        /// <summary>
        /// Returns program help
        /// </summary>
        /// <returns></returns>
        string GetProgramHelp();

        /// <summary>
        /// Returns program version
        /// </summary>
        /// <returns></returns>
        string GetVersion();

        /// <summary>
        /// Creates file with appropriate filename
        /// </summary>
        /// <param name="fileName"></param>
        void CreateFile(string fileName);

        /// <summary>
        /// Returns true, if exit was entered by user
        /// </summary>
        bool IsExit
        {
            get;
        }
    }

}