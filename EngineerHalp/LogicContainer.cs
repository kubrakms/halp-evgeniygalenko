﻿// -----------------------------------------------------------------------
// <copyright file="" company="Soft Serve Academy">
//This code was be written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.EngineerHalp
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;    
    using Autofac;    
    using SoftServe.Halp.EngineerHalp.Controllers;
    using SoftServe.Halp.EngineerHalp.Abstractions;
    using SoftServe.Halp.CalculatorModule.Controllers;
    using SoftServe.Halp.CalculatorModule.Abstractions;
   


    /// <summary>
    /// Provides service to creation custom modules with application's logic
    /// </summary>
    public class LogicContainer : IServiceProvider
    {
        static LogicContainer()
        {          
            Mapper.CreateMap<object, TypedParameter>().ConvertUsing(new TypedParameterTypeConverter());
        }

        private LogicContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<UtilController>().As<IUtilContract>();
            builder.RegisterType<IOController>().As<IIOContract>();
            m_Container = builder.Build();
        }

        #region Nested private logic

        private class TypedParameterTypeConverter : ITypeConverter<object, TypedParameter>
        {
            public TypedParameter Convert(ResolutionContext context)
            {
                if (context.SourceValue is System.Globalization.CultureInfo)
                    return new TypedParameter(typeof(System.Globalization.CultureInfo), context.SourceValue);

                return new TypedParameter(context.SourceType, context.SourceValue);
            }
        }

        #endregion

        #region IServiceProvider implementation

        public object GetService(Type serviceType)
        {
            if (serviceType is IUtilContract)
                return m_Container.Resolve<IUtilContract>();
            if (serviceType is IIOContract)
                return m_Container.Resolve<IIOContract>();

            return null;
        }

        #endregion

        #region Thread safe singletone

        public static LogicContainer Instance
        {
            get
            {
                lock (__mutex)
                {
                    if (m_Instance == null)
                        m_Instance = new LogicContainer();
                }
                return m_Instance;
            }
        }

        #endregion

        public T GetService<T>()
        {
            return m_Container.Resolve<T>();            
        }

        public T GetService<T>(object[] parameters)
        {
            if (parameters == null)
                return GetService<T>();

            var @params = Mapper.Map<object[], IEnumerable<TypedParameter>>(parameters);
            return m_Container.Resolve<T>(@params);
        }

        private static readonly object __mutex = new object();
        private static LogicContainer m_Instance = default(LogicContainer);
        private readonly IContainer m_Container = default(IContainer);
    }
}
